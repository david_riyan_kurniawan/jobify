import '../widgets/responbilities_item.dart';
import '../widgets/requirements_item.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:readmore/readmore.dart'; //untuk menambahkan widget read more pada aplikasi

// ignore: must_be_immutable
class JobDescription extends StatelessWidget {
  JobDescription({super.key});
  String desciption =
      'Senior UI/UX Designer needed, for collaborate with team and developer as full time designer. by having good communication skills,mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(children: [
            const SizedBox(
              height: 26,
            ),
            //membuat job description
            Text('Job Description',
                style: GoogleFonts.plusJakartaSans(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            const SizedBox(
              height: 25,
            ),
            ReadMoreText(
              desciption, //value yang mengembalikan nilai string
              style: GoogleFonts.plusJakartaSans(
                  fontSize: 14, color: const Color(0xff7F879E)),
              trimLines: 3,
              trimMode: TrimMode.Line,
              trimCollapsedText: 'Read More',
              trimExpandedText: ' ..Show Less',
              lessStyle: GoogleFonts.plusJakartaSans(
                  fontSize: 14, color: const Color(0xff3860E2)),
              moreStyle: GoogleFonts.plusJakartaSans(
                  fontSize: 14, color: const Color(0xff3860E2)),
            ),
            const SizedBox(
              height: 26,
            ),
            //membuat fitur responbilites
            Text(
              'Responsibilities',
              style: GoogleFonts.plusJakartaSans(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
            const SizedBox(
              height: 24,
            ),
            //membuat content atribut atau komponen responbilities
            const ResponbilitiesItem(),
            const SizedBox(
              height: 26,
            ),
            //membuat fitur Requirements
            Text('Requirements',
                style: GoogleFonts.plusJakartaSans(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            const SizedBox(
              height: 22,
            ),
            //atribut atau komponen Requirements
            const RequirementsItem()
          ]),
          //membuat button apply
          Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 150,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                        Colors.white24,
                        Colors.white30,
                        Colors.white54,
                        Colors.white
                      ])),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 46,
                    width: 327,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: const Color(0xff3860E2)),
                    child: Material(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {},
                        borderRadius: BorderRadius.circular(8),
                        child: Center(
                          child: Text(
                            'Apply Now',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.plusJakartaSans(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import '../widgets/content_starup_item.dart';
import './job_description.dart';

class JobDetail extends StatelessWidget {
  const JobDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // toolbarHeight: 50,
        iconTheme: const IconThemeData(color: Colors.black),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Job Detail',
          style: GoogleFonts.plusJakartaSans(
              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 24.0),
            child: SvgPicture.asset('assets/svgs/icon-favorite.svg'),
          )
        ],
      ),
      body: DefaultTabController(
        length: 3,
        child: Center(
          child: SizedBox(
            width: 327,
            child: NestedScrollView(
              headerSliverBuilder: (context, innerBoxIsScrolled) => [
                const SliverToBoxAdapter(
                  child:
                      ContentStarupItem(), //file yang berisi content yang akan ditampilkan diatas nested scroll view
                ),
                SliverAppBar(
                  backgroundColor: Colors.white,
                  pinned: true,
                  elevation: 0,
                  bottom: PreferredSize(
                    preferredSize: const Size.fromHeight(0),
                    child: TabBar(
                        indicatorColor: const Color(0xff3860E2),
                        labelColor: const Color(0xff3860E2),
                        unselectedLabelColor: const Color(0xff7F879E),
                        tabs: [
                          Tab(
                            child: Text(
                              'Summary',
                              style: GoogleFonts.plusJakartaSans(fontSize: 14),
                            ),
                          ),
                          Tab(
                            child: Text(
                              'Activity',
                              style: GoogleFonts.plusJakartaSans(fontSize: 14),
                            ),
                          ),
                          Tab(
                            child: Text(
                              'About',
                              style: GoogleFonts.plusJakartaSans(fontSize: 14),
                            ),
                          )
                        ]),
                  ),
                )
              ],
              body: TabBarView(
                children: [JobDescription(), Container(), Container()],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:slicing/pages/job_applied.dart';
import '../widgets/title_item.dart';
import '../widgets/form_item.dart';
import '../widgets/row_sugestion.dart';
import '../widgets/work_content_item.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late int index;
  List showWidget = [
    Center(
      child: HomeScreen(),
    ),
    Center(
      child: Container(),
    ),
    Center(
      child: JobApplied(),
    ),
    Center(
      child: Container(),
    ),
    Center(
      child: Container(),
    ),
  ];
  @override
  //membuat function untuk menampung variable index
  void initState() {
    index = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Stack(
            children: [
              //membuat latar belakang
              Container(
                width: double.infinity,
                height: 241,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/latar-belakang.png'),
                        fit: BoxFit.cover)),
              ),
              //membuat title,notifikasi icon, dan foto profile
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 74.0, left: 24, right: 24),
                    child: TitleNotifProfileItem(),
                  ),
                  //membuat card yang berguna untuk form search,location,dan button
                  Padding(
                      padding: EdgeInsets.only(top: 72.0, left: 24, right: 24),
                      child: FormItem()),
                  //membuat row sugestion dan see all
                  Padding(
                    padding: EdgeInsets.only(top: 20.0, left: 24, right: 24),
                    child: RowSuggestionItem(),
                  ),
                  //membuat card yang berisi content pekerjaan/ atau daftar pekerjaan
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 26.0, left: 24, right: 24),
                    child: WorkContentItem(),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:slicing/pages/home_screen.dart';
import 'package:slicing/pages/job_applied.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({super.key});

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  late int index;

/*membuat list index, yang nantinya akan di tampilkan pada halaman
 ketika kita klik menu pada bottom navigation bar nya*/
  List showWidget = [
    const HomeScreen(),
    Container(),
    const JobApplied(),
    Container(),
    Container()
  ];

  @override
  //membuat function untuk menampung variable index
  void initState() {
    index = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: showWidget[index],
      bottomNavigationBar: BottomNavigationBar(
          onTap: (value) {
            //membuat setState, karna kita hendak melakukan perubahan pada tampilan
            setState(() {
              index = value;
            });
          },
          currentIndex: index,
          selectedLabelStyle: GoogleFonts.plusJakartaSans(
              fontSize: 12, fontWeight: FontWeight.bold),
          selectedItemColor: const Color(0xff1B2124),
          unselectedLabelStyle: GoogleFonts.plusJakartaSans(
            fontSize: 12,
            color: const Color(0xff7F879E),
          ),
          showUnselectedLabels: true,
          unselectedItemColor: const Color(0xff7F879E),
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  'assets/svgs/icon-home.svg',
                  // color: Color(0xff7F879E),
                ),
                label: 'Home'),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  'assets/svgs/icon-simpan.svg',
                  color: const Color(0xff7F879E),
                ),
                label: 'Saved'),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  'assets/svgs/icon-applied.svg',
                  color: const Color(0xff7F879E),
                ),
                label: 'Applied'),
            BottomNavigationBarItem(
                icon: Image.asset('assets/images/icon-messages.png'),
                label: 'Messages'),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  'assets/svgs/icon-profile.svg',
                  color: const Color(0xff7F879E),
                ),
                label: 'Profile'),
          ]),
    );
  }
}

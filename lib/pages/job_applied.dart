import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class JobApplied extends StatelessWidget {
  const JobApplied({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: [
        Column(children: [
          Stack(children: [
            Container(
              height: 146,
              width: double.infinity,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/latar-belakang.png'),
                      fit: BoxFit.cover)),
            ),
            Padding(
              padding:
                  // ignore: unnecessary_const
                  const EdgeInsets.only(top: 74.0, left: 24, right: 24),
              child: Row(
                children: [
                  Text(
                    'Job Applied',
                    style: GoogleFonts.plusJakartaSans(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 22),
                  ),
                  const Spacer(),
                  SvgPicture.asset('assets/svgs/icon-notifikasi.svg'),
                  const Padding(
                    padding: EdgeInsets.only(left: 24.0),
                    child: SizedBox(
                      width: 42,
                      height: 42,
                      child: CircleAvatar(
                        backgroundColor: Color(0xff67C1F4),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ]),
          const SizedBox(
            height: 35,
          ),
          //row my aplication
          Padding(
            padding: const EdgeInsets.only(left: 24.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'My Application',
                  style: GoogleFonts.plusJakartaSans(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: const Color(0xFF1B2124)),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Row(
                    children: [
                      Text(
                        'All',
                        style: GoogleFonts.plusJakartaSans(
                            fontSize: 14, color: const Color(0xff7F879E)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 24),
                        child: SvgPicture.asset('assets/svgs/icon-row.svg'),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 35,
          ),
          //work content pertama
          Container(
            height: 267,
            width: 327,
            decoration: const BoxDecoration(color: Colors.white),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: SizedBox(
                            height: 48,
                            width: 48,
                            child: SvgPicture.asset(
                                'assets/svgs/logo-starup.svg')),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Column(
                          children: [
                            Text(
                              'Pinterest',
                              style: GoogleFonts.plusJakartaSans(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: const Color(0xff1B2124)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 2.0),
                              child: Text(
                                'Senior UI/UX Designer',
                                style: GoogleFonts.plusJakartaSans(
                                    fontSize: 12,
                                    color: const Color(0xff7F879E)),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                          height: 27,
                          width: 72,
                          child:
                              SvgPicture.asset('assets/svgs/icon-pending.svg')),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 28.0,
                  ),
                  child: GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: SizedBox(
                                height: 20,
                                width: 20,
                                child: SvgPicture.asset(
                                    'assets/svgs/icon-location.svg'),
                              ),
                            ),
                            Text(
                              'California, Freelance ,WFO',
                              style: GoogleFonts.plusJakartaSans(
                                  fontSize: 12, color: const Color(0xff7F879E)),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0, right: 24),
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 5.0),
                                child: SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: SvgPicture.asset(
                                        'assets/svgs/icon-salary.svg')),
                              ),
                              Text('Rp.30.000.000',
                                  style: GoogleFonts.plusJakartaSans(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600)),
                              Text(
                                '/Month',
                                style: GoogleFonts.plusJakartaSans(
                                    fontSize: 12,
                                    color: const Color(0xff7F879E)),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          'Senior UI/UX Designer needed, for collaborate with team and developer as full time designer. by having good communication skills,',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.plusJakartaSans(
                              fontSize: 12, color: const Color(0xff7F879E)),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 38,
                  width: 355,
                  decoration: BoxDecoration(
                      border:
                          Border.all(width: 2, color: const Color(0xff3860E2)),
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white),
                  child: Material(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {},
                      borderRadius: BorderRadius.circular(8),
                      child: Center(
                        child: Text(
                          'Applied',
                          style: GoogleFonts.plusJakartaSans(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: const Color(0xff3860E2)),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          //work content kedua
          Container(
            height: 267,
            width: 327,
            decoration: const BoxDecoration(color: Colors.white),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: SizedBox(
                            height: 48,
                            width: 48,
                            child: SvgPicture.asset(
                                'assets/svgs/icon-discord.svg')),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Column(
                          children: [
                            Text(
                              'Discord',
                              style: GoogleFonts.plusJakartaSans(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: const Color(0xff1B2124)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 2.0),
                              child: Text(
                                'Junior UI Designer',
                                style: GoogleFonts.plusJakartaSans(
                                    fontSize: 12,
                                    color: const Color(0xff7F879E)),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                          height: 27,
                          width: 72,
                          child: SvgPicture.asset(
                              'assets/svgs/icon-approved.svg')),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 28.0,
                  ),
                  child: GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: SizedBox(
                                height: 20,
                                width: 20,
                                child: SvgPicture.asset(
                                    'assets/svgs/icon-location.svg'),
                              ),
                            ),
                            Text(
                              'Purwokerto, Contract, WFO',
                              style: GoogleFonts.plusJakartaSans(
                                  fontSize: 12, color: const Color(0xff7F879E)),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0, right: 24),
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 5.0),
                                child: SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: SvgPicture.asset(
                                        'assets/svgs/icon-salary.svg')),
                              ),
                              Text('Rp.10.000.000',
                                  style: GoogleFonts.plusJakartaSans(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600)),
                              Text(
                                '/Month',
                                style: GoogleFonts.plusJakartaSans(
                                    fontSize: 12,
                                    color: const Color(0xff7F879E)),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          'Junior UI Designer needed needed, for redesign many page in discord web, desktop and mobile app so...',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.plusJakartaSans(
                              fontSize: 12, color: const Color(0xff7F879E)),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                // Container(
                //   height: 38,
                //   width: 355,
                //   decoration: BoxDecoration(
                //       border: Border.all(
                //           width: 2, color: const Color(0xff3860E2)),
                //       borderRadius: BorderRadius.circular(8),
                //       color: Colors.white),
                //   child: Material(
                //     borderRadius: BorderRadius.circular(8),
                //     color: Colors.transparent,
                //     child: InkWell(
                //       onTap: () {},
                //       borderRadius: BorderRadius.circular(8),
                //       child: Center(
                //         child: Text(
                //           'Applied',
                //           style: GoogleFonts.plusJakartaSans(
                //               fontWeight: FontWeight.bold,
                //               fontSize: 12,
                //               color: const Color(0xff3860E2)),
                //         ),
                //       ),
                //     ),
                //   ),
                // )
              ],
            ),
          )
        ])
      ],
    ));
  }
}

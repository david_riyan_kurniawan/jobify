import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ResponbilitiesItem extends StatelessWidget {
  const ResponbilitiesItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'Collaborate with product manager and teach throughout the design life-cycle such as product wireframes ',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'Design new product, new interfaces and experience.',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'Create a design theme that promotes a strong brand affiliation.',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'Hands-on experience with creating short videos and editing',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

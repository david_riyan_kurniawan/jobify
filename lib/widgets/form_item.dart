import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class FormItem extends StatelessWidget {
  const FormItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 188,
      width: 327,
      child: Card(
        elevation: 0.2,
        color: Colors.white,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: TextField(
                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  icon: SvgPicture.asset('assets/svgs/search-icon.svg'),
                  fillColor: Colors.white,
                  filled: true,
                  // prefix: Container(
                  //   height: 24,
                  //   width: 24,
                  //   child: SvgPicture.asset(
                  //       'assets/svgs/search-icon.svg'),
                  // ),
                  hintText: 'Search job, company etc',
                  hintStyle: GoogleFonts.plusJakartaSans(fontSize: 14),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: TextField(
                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  icon: SvgPicture.asset('assets/svgs/icon-location.svg'),
                  fillColor: Colors.white,
                  filled: true,
                  // prefix: Container(
                  //   height: 24,
                  //   width: 24,
                  //   child: SvgPicture.asset(''),
                  // ),
                  hintText: 'Location',
                  hintStyle: GoogleFonts.plusJakartaSans(fontSize: 14),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 14.0),
              child: Container(
                width: 299,
                height: 46,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: const Color(0xFF3860E2)),
                child: Material(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {},
                    child: Center(
                      child: Text(
                        'Button',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.plusJakartaSans(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

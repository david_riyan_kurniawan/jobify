import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class TitleNotifProfileItem extends StatelessWidget {
  const TitleNotifProfileItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'Jobify',
          style: GoogleFonts.plusJakartaSans(
              fontWeight: FontWeight.bold, color: Colors.white, fontSize: 22),
        ),
        Text(
          '.',
          style: GoogleFonts.plusJakartaSans(
              fontWeight: FontWeight.bold, color: Colors.amber, fontSize: 22),
        ),
        const Spacer(),
        SvgPicture.asset('assets/svgs/icon-notifikasi.svg'),
        const Padding(
          padding: EdgeInsets.only(left: 24.0),
          child: SizedBox(
            width: 42,
            height: 42,
            child: CircleAvatar(
              backgroundColor: Colors.blue,
            ),
          ),
        )
      ],
    );
  }
}

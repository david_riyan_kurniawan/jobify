import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import '../pages/job_detail.dart';

class WorkContentItem extends StatefulWidget {
  const WorkContentItem({
    super.key,
  });

  @override
  State<WorkContentItem> createState() => _WorkContentItemState();
}

class _WorkContentItemState extends State<WorkContentItem> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //content jobs pertama
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                    child: const JobDetail(),
                    type: PageTransitionType.rightToLeft));
          },
          child: Container(
            height: 267,
            width: 327,
            decoration: const BoxDecoration(color: Colors.white),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                        height: 48,
                        width: 48,
                        child: SvgPicture.asset('assets/svgs/logo-starup.svg')),
                    Column(
                      children: [
                        Text(
                          'Pinterest',
                          style: GoogleFonts.plusJakartaSans(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              color: const Color(0xff1B2124)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0),
                          child: Text(
                            'Senior UI/UX Designer',
                            style: GoogleFonts.plusJakartaSans(
                                fontSize: 12, color: const Color(0xff7F879E)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                        height: 24,
                        width: 24,
                        child: SvgPicture.asset('assets/svgs/icon-save.svg')),
                  ],
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 28.0, left: 24, right: 24),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 5.0),
                            child: SizedBox(
                              height: 20,
                              width: 20,
                              child: SvgPicture.asset(
                                  'assets/svgs/icon-location.svg'),
                            ),
                          ),
                          Text(
                            'California, Freelance ,WFO',
                            style: GoogleFonts.plusJakartaSans(
                                fontSize: 12, color: const Color(0xff7F879E)),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0, right: 24),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: SvgPicture.asset(
                                      'assets/svgs/icon-salary.svg')),
                            ),
                            Text('Rp.30.000.000',
                                style: GoogleFonts.plusJakartaSans(
                                    fontSize: 12, fontWeight: FontWeight.w600)),
                            Text(
                              '/Month',
                              style: GoogleFonts.plusJakartaSans(
                                  fontSize: 12, color: const Color(0xff7F879E)),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Senior UI/UX Designer needed, for collaborate with team and developer as full time designer. by having good communication skills,',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: GoogleFonts.plusJakartaSans(
                            fontSize: 12, color: const Color(0xff7F879E)),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 38,
                  width: 299,
                  decoration: BoxDecoration(
                      border:
                          Border.all(width: 2, color: const Color(0xff3860E2)),
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white),
                  child: Material(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {},
                      borderRadius: BorderRadius.circular(8),
                      child: Center(
                        child: Text(
                          'Apply Now',
                          style: GoogleFonts.plusJakartaSans(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: const Color(0xff3860E2)),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),

        //konten jobs kedua
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                    duration: const Duration(milliseconds: 330),
                    child: const JobDetail(),
                    type: PageTransitionType.rightToLeft));
          },
          child: Container(
            height: 267,
            width: 327,
            decoration: const BoxDecoration(color: Colors.white),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                        height: 48,
                        width: 48,
                        child: SvgPicture.asset('assets/svgs/logo-starup.svg')),
                    Column(
                      children: [
                        Text(
                          'Pinterest',
                          style: GoogleFonts.plusJakartaSans(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              color: const Color(0xff1B2124)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0),
                          child: Text(
                            'Senior UI/UX Designer',
                            style: GoogleFonts.plusJakartaSans(
                                fontSize: 12, color: const Color(0xff7F879E)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                        height: 24,
                        width: 24,
                        child: SvgPicture.asset('assets/svgs/icon-save.svg')),
                  ],
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 28.0, left: 24, right: 24),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 5.0),
                            child: SizedBox(
                              height: 20,
                              width: 20,
                              child: SvgPicture.asset(
                                  'assets/svgs/icon-location.svg'),
                            ),
                          ),
                          Text(
                            'California, Freelance ,WFO',
                            style: GoogleFonts.plusJakartaSans(
                                fontSize: 12, color: const Color(0xff7F879E)),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0, right: 24),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: SvgPicture.asset(
                                      'assets/svgs/icon-salary.svg')),
                            ),
                            Text('Rp.30.000.000',
                                style: GoogleFonts.plusJakartaSans(
                                    fontSize: 12, fontWeight: FontWeight.w600)),
                            Text(
                              '/Month',
                              style: GoogleFonts.plusJakartaSans(
                                  fontSize: 12, color: const Color(0xff7F879E)),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Senior UI/UX Designer needed, for collaborate with team and developer as full time designer. by having good communication skills,',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: GoogleFonts.plusJakartaSans(
                            fontSize: 12, color: const Color(0xff7F879E)),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 38,
                  width: 299,
                  decoration: BoxDecoration(
                      border:
                          Border.all(width: 2, color: const Color(0xff3860E2)),
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white),
                  child: Material(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {},
                      borderRadius: BorderRadius.circular(8),
                      child: Center(
                        child: Text(
                          'Apply Now',
                          style: GoogleFonts.plusJakartaSans(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: const Color(0xff3860E2)),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

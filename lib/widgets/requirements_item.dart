import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class RequirementsItem extends StatelessWidget {
  const RequirementsItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'On-site in California',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'Have good communication skills and team working skill.',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'Know the principal of animation and you can create high quality prototypes.',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset('assets/svgs/icon-list.svg')),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                ),
                child: Text(
                  'Figma, Xd & Sketch know about this app.',
                  maxLines: 3,
                  softWrap: true,
                  style: GoogleFonts.plusJakartaSans(
                      fontSize: 14, color: const Color(0xff7F879E)),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

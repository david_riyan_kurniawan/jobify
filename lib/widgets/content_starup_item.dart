/*
file ini berisi konten-konten yang nantinya akan ditampilkan diatas
widget nested scroll view,
file ini berisikan logo star up,nama star up,posisi pekerjaan,dan juga
row lokasi,salary,jumlah favorite
 */

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ContentStarupItem extends StatelessWidget {
  const ContentStarupItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 50,
        ),
        SizedBox(
            height: 84,
            width: 84,
            child: SvgPicture.asset('assets/svgs/logo-starup.svg')),
        const SizedBox(
          height: 24,
        ),
        Text('Pinterest',
            style: GoogleFonts.plusJakartaSans(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black)),
        const SizedBox(
          height: 8,
        ),
        Text(
          'Senior UI/UX Designer',
          style: GoogleFonts.plusJakartaSans(
              fontSize: 14, color: const Color(0xff7F879E)),
        ),
        //membuat row alamat,salary,dan juga jumlah favorite
        const SizedBox(
          height: 28,
        ),
        Container(
          height: 60,
          width: 327,
          decoration: BoxDecoration(
              border: Border.all(color: const Color(0xffF1F1FA)),
              borderRadius: BorderRadius.circular(8)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SvgPicture.asset(
                'assets/svgs/icon-location.svg',
                color: const Color(0xffF59A74),
              ),
              Text(
                'California',
                style: GoogleFonts.plusJakartaSans(
                    fontSize: 12, color: const Color(0xff7F879E)),
              ),
              SvgPicture.asset(
                'assets/svgs/icon-salary.svg',
                color: const Color(0xffF59A74),
              ),
              Row(
                children: [
                  Text(
                    'Rp.30jt',
                    style: GoogleFonts.plusJakartaSans(
                        fontSize: 12, fontWeight: FontWeight.w500),
                  ),
                  Text(
                    '/Mo',
                    style: GoogleFonts.plusJakartaSans(
                        fontSize: 12, color: const Color(0xff7F879E)),
                  )
                ],
              ),
              SvgPicture.asset(
                'assets/svgs/icon-favorite.svg',
                color: const Color(0xffF59A74),
              ),
              Text(
                '210 Saved',
                style: GoogleFonts.plusJakartaSans(
                    fontSize: 12, color: const Color(0xff7F879E)),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

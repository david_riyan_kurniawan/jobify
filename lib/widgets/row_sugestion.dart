import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RowSuggestionItem extends StatelessWidget {
  const RowSuggestionItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Suggested Job',
          style: GoogleFonts.plusJakartaSans(
              fontWeight: FontWeight.bold,
              fontSize: 18,
              color: const Color(0xFF1B2124)),
        ),
        GestureDetector(
          onTap: () {},
          child: Text(
            'See All',
            style: GoogleFonts.plusJakartaSans(
                fontSize: 14, color: const Color(0xff7F879E)),
          ),
        )
      ],
    );
  }
}
